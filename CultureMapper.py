#!/usr/bin/env python3

import glob
import math
import os #Used for relative paths, if needed
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import sys
import timeit

mod_dir = sys.argv[1]
if not mod_dir.endswith("/"):
	mod_dir += "/"
output = sys.argv[2]
names_output = sys.argv[3]
cultures_loc = sys.argv[4] if len(sys.argv) > 4 else None

baronydict = {}
colordict = {}
countydict = {}

def add_to_baronydict(line):
	lsplit = line.strip().split(";")
	color = (int(lsplit[1]), int(lsplit[2]), int(lsplit[3]))
	prov = int(lsplit[0])
	baronydict[prov] = color
	colordict[color] = prov

def color_from_line(line):
	startidx = line.find("{") + 1
	endidx = line.rfind("}")
	if "." in line:
		return tuple(int(255 * float(token)) for token in line[startidx:endidx].strip().split())
	else:
		return tuple(int(token) for token in line[startidx:endidx].strip().split())
	

start_time = timeit.default_timer()

with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if "Impassable Ocean" in line:
			pass
		elif "Impassable" in line:
			pass
		elif "Ocean" in line or ";ocean_" in line or "Lake" in line or ";lake_" in line or "River" in line or ";river_" in line:
			pass
		elif line[0] == "#":
			pass
		else:
			add_to_baronydict(line)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[CultureMapper] Took " + str(round(elapsed_time * 1000)) + " ms for defs")

basedir = mod_dir + "common/culture/cultures"
culture_files = list(s for s in os.listdir(mod_dir + "common/culture/cultures"))
#print(landed_titles)

culture_colors = {}
used_cultures = {}
heritages = {}

for file in culture_files:
	with open(basedir + "/" + file, encoding = "utf-8") as f:
		i = 0
		curr_id = ""
		for line in f.readlines():
			lsplit = line.split()
			if i == 0:
				curr_id = lsplit[0].strip("\ufeff")
			elif i == 1 and curr_id == "":
				curr_id = lsplit[0].strip("\ufeff")
			elif "color" in line and "#color" not in line: # thanks a lot steelfolk
				for token in lsplit:
					if token == "color":
						culture_colors[curr_id] = color_from_line(line)
				#break
			elif "language" in line:
				for token in lsplit:
					if token != "language":
						heritages[curr_id] = token
				break
			i += 1

prov_cultures = {}
heritages_rev = {}
for k, v in heritages.items():
	if v not in heritages_rev.keys():
		heritages_rev[v] = culture_colors[k]

prov_files = list(mod_dir + "history/provinces/" + s for s in os.listdir(mod_dir + "history/provinces"))

for prov_file in prov_files:
	curr_prov = 0
	with open(prov_file, encoding = "utf-8") as f:
		for line in f.readlines():
			lsplit = line.split()
			if "{" in line and "." not in line and "buildings" not in line and not line.startswith("#"):
				curr_prov = int(lsplit[0])
			elif "culture" in line:
				prov_cultures[curr_prov] = lsplit[-1]

#print(prov_cultures)
image = Image.open(mod_dir + "map_data/provinces.png")
name_height = image.size[1]
name_width = 100 + (420 * math.ceil(len(culture_colors) / ((image.size[1] - 200) // 50)))
image2 = Image.new('RGB', (image.size[0], image.size[1]), color = 'red')
image3 = Image.new('RGBA', (name_width, name_height), color = (200, 200, 200, 255))

start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()

for i in range(image.size[0]):
	for j in range(image.size[1]):
		try:
			px = pixels[i, j]
			c = (px[0], px[1], px[2])
			id = colordict[c]
			culture = prov_cultures[id]
			pixels2[i, j] = culture_colors[culture]#heritages_rev[heritages[culture]]
			used_cultures[culture] = culture_colors[culture]
		except:
			pixels2[i, j] = (255, 255, 255)

draw = ImageDraw.Draw(image3)
font = ImageFont.truetype("LiberationMono-Regular.ttf", 40)
draw.fontmode = "1"

index = 0
ncs = list((k, v) for k, v in culture_colors.items())
ncs.sort(key = lambda k: k[0])

class IdentityDict(dict):
	def __missing__(self, key):
		return key


if cultures_loc is not None:
	ncs2 = []
	display_names = {}
	for path in glob.glob(cultures_loc + "/**", recursive = True):
		if not os.path.isfile(path):
			continue
		with open(path, "r") as f:
			for line in f.readlines():
				if ":0" in line and "collective" not in line and "group" not in line:
					tokens = line.split()
					identifier = tokens[0][:-2]
					if identifier not in culture_colors.keys():
						continue
					name = "".join(t + " " for t in tokens[1:])[1:-2]
					ncs2.append((identifier, culture_colors[identifier]))
					display_names[identifier] = name
	ncs = ncs2
else:
	display_names = IdentityDict()



for code_name, color in ncs:
	if code_name in used_cultures.keys():
		name = display_names[code_name]
		"""if index < 200:
			draw.text((((index // 20) * 300) + 3600,
						(20 + (index % 20) * 60)),
						name,
						fill = color,
						font = font
					)
		elif index < 260:
			draw.text((((index // 20) * 500) + 4200,
						(20 + (index % 20) * 60)),
						name,
						fill = color,
						font = font
					)
		else:
			draw.text(((((index - 260) // 16) * 300) + 6500,
						((index - 260) % 16) * 60 + (image.size[1] - 1000)),
						name,
						fill = color,
						font = font
					)
		index += 1"""
		height = (image.size[1] - 200) // 50
		xpos = 420 * (index // height) + 100
		ypos = 50 * (index % height) + 100
		draw.rectangle(xy = [xpos, ypos, xpos + 40, ypos + 40], fill = color, outline = (0, 0, 0), width = 2)
		draw.text((xpos + 55, ypos),
						name,
						fill = (0, 0, 0),
						font = font
		)
		index += 1

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[CultureMapper] Took " + str(round(elapsed_time * 1000)) + " ms for image")

#image2.show()
image2.save(output, "PNG")
image3.save(names_output, "PNG")