#!/usr/bin/env python3

import os #Used for relative paths, if needed
from PIL import Image
import sys
import timeit

baronydict = {}
colordict = {}
countydict = {}

mod_dir = sys.argv[1]
if not mod_dir.endswith("/"):
	mod_dir += "/"
output = sys.argv[2]

def add_to_baronydict(line):
	lsplit = line.strip().split(";")
	color = (int(lsplit[1]), int(lsplit[2]), int(lsplit[3]))
	prov = int(lsplit[0])
	baronydict[prov] = color
	colordict[color] = prov

def color_from_line(line):
	startidx = line.find("{") + 1
	endidx = line.rfind("}")
	if "." in line:
		return tuple(int(255 * float(token)) for token in line[startidx:endidx].strip().split())
	else:
		return tuple(int(token) for token in line[startidx:endidx].strip().split())
	

start_time = timeit.default_timer()

with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if "Impassable Ocean" in line:
			pass
		elif "Impassable" in line:
			pass
		elif "Ocean" in line or ";ocean_" in line or "Lake" in line or ";lake_" in line or "River" in line or ";river_" in line:
			pass
		elif line[0] == "#":
			pass
		else:
			add_to_baronydict(line)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("Took " + str(round(elapsed_time * 1000)) + " ms for defs")

holding_colors = {"none": (120, 120, 120), "castle_holding": (119, 181, 206), "city_holding": (255, 204, 194), "metropolis_holding": (255, 25, 71), "tribal_holding": (127, 75, 66), "church_holding": (204, 204, 204)}

prov_holdings = {}

prov_files = list(mod_dir + "history/provinces/" + s for s in os.listdir(mod_dir + "history/provinces"))

for prov_file in prov_files:
	curr_prov = 0
	with open(prov_file, encoding = "utf-8") as f:
		for line in f.readlines():
			lsplit = line.split()
			if "{" in line and "." not in line and "buildings" not in line and not line.startswith("#"):
				curr_prov = int(lsplit[0])
			elif "holding" in line:
				prov_holdings[curr_prov] = lsplit[-1]

image = Image.open(mod_dir + "map_data/provinces.png")
image2 = Image.new('RGB', (image.size[0], image.size[1]), color = 'red')

start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()

for i in range(image.size[0]):
	for j in range(image.size[1]):
		try:
			px = pixels[i, j]
			c = (px[0], px[1], px[2])
			id = colordict[c]
			culture = prov_holdings[id]
			pixels2[i, j] = holding_colors[culture]
		except:
			pixels2[i, j] = (255, 255, 255)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("Took " + str(round(elapsed_time * 1000)) + " ms for image")

image2.save(output, "PNG")