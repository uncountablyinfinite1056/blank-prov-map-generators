#!/usr/bin/env python3

import os
import sys
import timeit
from PIL import Image

colordict = {}

mod_dir = sys.argv[1]
if not mod_dir.endswith("/"):
	mod_dir += "/"
output = sys.argv[2]
only_special = "--only-special" in sys.argv


def line_to_color(line):
	lsplit = line.strip().split(";")
	color = (int(lsplit[1]), int(lsplit[2]), int(lsplit[3]))
	return color

start_time = timeit.default_timer()

"""with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if "Impassable Ocean" in line:
			#print("Ocean")
			#print(line_to_color(line))
			colordict[line_to_color(line)] = (51, 67, 85)
		elif "Impassable" in line:
			colordict[line_to_color(line)] = (30, 30, 30)
		elif "Ocean" in line or ";ocean_" in line or "Lake" in line or ";lake_" in line or "River" in line or ";river_" in line:
			colordict[line_to_color(line)] = (51, 67, 85)"""

water_provs = set()
impassable_provs = set()

with open(mod_dir + "map_data/default.map") as f:
	for line in f:
		water = False
		impassable = False
		if "sea_zones" in line or "lakes" in line or "river_provinces" in line or "impassable_seas" in line:
			water = True
		elif "impassable_mountains" in line:
			impassable = True

		tokens = line.split()
		if "LIST" in line:
			try:
				for token in tokens[tokens.index("{") + 1:tokens.index("}")]:
					if water:
						water_provs.add(int(token))
					elif impassable:
						impassable_provs.add(int(token))
			except:
				pass
		elif "RANGE" in line:
			for pid in range(int(tokens[4]), int(tokens[5]) + 1):
				if water:
					water_provs.add(int(pid))
				elif impassable:
					impassable_provs.add(int(pid))


with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if line[0] == "#":
			continue
		pid = int(line.split(";")[0])
		if pid in water_provs:
			#print("Ocean")
			#print(line_to_color(line))
			colordict[line_to_color(line)] = (51, 67, 85)
		elif pid in impassable_provs:
			colordict[line_to_color(line)] = (30, 30, 30)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[ProvCleaner] Took " + str(round(elapsed_time * 1000)) + " ms for defs")

image = Image.open(mod_dir + "map_data/provinces.png")
image2 = Image.new('RGBA', (image.size[0], image.size[1]), color = (255, 0, 0, 0))

#print(colordict)
# 185;187;127 override
# 135;224;71
# 248;62;71
# 242;199;32
# 218;201;50
# 226;130;30
# 224;127;168
start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()

for i in range(image.size[0]):
	for j in range(image.size[1]):
		try:
			px = pixels[i, j]
			c = (px[0], px[1], px[2])
			pixels2[i, j] = colordict[c]
		except:
			if only_special:
				pass#pixels2[i, j] = (255, 255, 255, 0)
			else:
				pixels2[i, j] = pixels[i, j]

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[ProvCleaner] Took " + str(round(elapsed_time * 1000)) + " ms for image")

#image2.show()
image2.save(output, "PNG")