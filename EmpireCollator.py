#!/usr/bin/env python3

import os
import sys
import timeit
from PIL import Image

baronydict = {}
colordict = {}
countydict = {}

mod_dir = sys.argv[1]
if not mod_dir.endswith("/"):
	mod_dir += "/"
output = sys.argv[2]

def add_to_baronydict(line):
	lsplit = line.strip().split(";")
	color = (int(lsplit[1]), int(lsplit[2]), int(lsplit[3]))
	prov = int(lsplit[0])
	baronydict[prov] = color
	colordict[color] = prov

def color_from_line(line):
	startidx = line.find("{") + 1
	endidx = line.rfind("}")
	if "." in line:
		return tuple(int(255 * float(token)) for token in line[startidx:endidx].strip().split())
	else:
		return tuple(int(token) for token in line[startidx:endidx].strip().split())
	

start_time = timeit.default_timer()

with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if "Impassable Ocean" in line:
			pass
		elif "Impassable" in line:
			pass
		elif "Ocean" in line or ";ocean_" in line or "Lake" in line or ";lake_" in line or "River" in line or ";river_" in line:
			pass
		elif line[0] == "#":
			pass
		else:
			add_to_baronydict(line)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[EmpireCollator] Took " + str(round(elapsed_time * 1000)) + " ms for defs")

basedir = mod_dir + "common/landed_titles"
landed_titles = list(s for s in os.listdir(mod_dir + "common/landed_titles"))
#print(landed_titles)

for file in landed_titles:
	with open(basedir + "/" + file, encoding = "utf-8") as f:
		curr_empire = ""
		in_barony = False
		past_empire = False
		curr_color = None
		for line in f.readlines():
			lsplit = line.split()
			for token in lsplit:
				if len(token) > 3 and token[0:2] == "e_":
					curr_empire = token
					in_barony = False
					past_empire = False
				elif len(token) > 3 and token[1:3] == "e_":
					curr_empire = token[1:]
					in_barony = False
					past_empire = False
				elif len(token) > 2 and token[0:2] == "b_":
					in_barony = True
				elif len(token) > 2 and (token[0:2] == "c_" or token[0:2] == "d_" or token[0:2] == "k_") and "name_list" not in line:
					past_empire = True
			if in_barony and "province" in line:
				for token in lsplit:
					try:
						id = int(token)
						countydict[id] = curr_color
					except:
						pass
			if curr_empire != "" and not in_barony and not past_empire and "color" in line and "#Color" not in line and "Colorado" not in line and "name_list" not in line:
				curr_color = color_from_line(line)

image = Image.open(mod_dir + "map_data/provinces.png")
image2 = Image.new('RGB', (image.size[0], image.size[1]), color = 'red')

start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()

for i in range(image.size[0]):
	for j in range(image.size[1]):
		try:
			px = pixels[i, j]
			c = (px[0], px[1], px[2])
			id = colordict[c]
			pixels2[i, j] = countydict[id]
		except:
			pixels2[i, j] = pixels[i, j]

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[EmpireCollator] Took " + str(round(elapsed_time * 1000)) + " ms for image")

#image2.show()
image2.save(output, "PNG")