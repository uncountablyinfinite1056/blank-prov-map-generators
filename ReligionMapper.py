#!/usr/bin/env python3

from collections import defaultdict
import glob
import math
import os #Used for relative paths, if needed
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import sys
import timeit

mod_dir = sys.argv[1]
if not mod_dir.endswith("/"):
	mod_dir += "/"
output = sys.argv[2]
names_output = sys.argv[3]
religions_loc = sys.argv[4] if len(sys.argv) > 4 else None

baronydict = {}
colordict = {}
countydict = {}

def add_to_baronydict(line):
	lsplit = line.strip().split(";")
	color = (int(lsplit[1]), int(lsplit[2]), int(lsplit[3]))
	prov = int(lsplit[0])
	baronydict[prov] = color
	colordict[color] = prov

def color_from_line(line):
	startidx = line.find("{") + 1
	endidx = line.rfind("}")
	if all(float(token) == 0 or float(token) >= 1 for token in line[startidx:endidx].strip().split()):
		return tuple(int(token) for token in line[startidx:endidx].strip().split())
	else:
		return tuple(255 * float(token) for token in line[startidx:endidx].strip().split())
	

start_time = timeit.default_timer()

with open(mod_dir + "map_data/definition.csv") as defs:
	for line in defs:
		if "Impassable Ocean" in line:
			pass
		elif "Impassable" in line:
			pass
		elif "Ocean" in line or ";ocean_" in line or "Lake" in line or ";lake_" in line or "River" in line or ";river_" in line:
			pass
		elif line[0] == "#":
			pass
		else:
			add_to_baronydict(line)

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[ReligionMapper] Took " + str(round(elapsed_time * 1000)) + " ms for defs")

basedir = mod_dir + "common/religion/religions"
religion_files = list(s for s in os.listdir(mod_dir + "common/religion/religions"))

religion_colors = {}
used_religions = {}
religion_families = defaultdict(list)

for file in religion_files:
	with open(basedir + "/" + file, encoding = "utf-8") as f:
		curr_faith = ""
		curr_rel = None
		for idx, line in enumerate(f.readlines()):
			lsplit = line.strip().strip("\ufeff").strip().split()
			if curr_rel is None and len(lsplit) > 0 and lsplit[0].endswith("religion"):
				curr_rel = lsplit[0]
			if "{" in line and "color" not in line and not "#" in lsplit[0]:
				curr_faith = lsplit[0]
			elif "color" in line and "." not in line:
				for token in lsplit:
					if token == "color":
						religion_families[curr_rel].append(curr_faith)
						religion_colors[curr_faith] = color_from_line(line)
			
#print(religion_families)
prov_religions = {}
#print(religion_colors)


prov_files = list(mod_dir + "history/provinces/" + s for s in os.listdir(mod_dir + "history/provinces"))

for prov_file in prov_files:
	curr_prov = 0
	with open(prov_file, encoding = "utf-8") as f:
		for line in f.readlines():
			lsplit = line.split()
			if "{" in line and "." not in line and "buildings" not in line and not line.startswith("#"):
				curr_prov = int(lsplit[0])
			elif "religion" in line:
				prov_religions[curr_prov] = lsplit[-1]

#print(prov_cultures)
image = Image.open(mod_dir + "map_data/provinces.png")
name_height = image.size[1]
name_width = 100 + (550 * math.floor((len(religion_colors) + len(religion_families)) / ((image.size[1] - 200) // 50)))
image2 = Image.new('RGB', (image.size[0], image.size[1]), color = 'red')
image3 = Image.new('RGBA', (name_width, name_height), color = (200, 200, 200, 255))

start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()

for i in range(image.size[0]):
	for j in range(image.size[1]):
		try:
			px = pixels[i, j]
			c = (px[0], px[1], px[2])
			id = colordict[c]
			religion = prov_religions[id]
			pixels2[i, j] = religion_colors[religion]
			used_religions[religion] = religion_colors[religion]
		except:
			pixels2[i, j] = (255, 255, 255)



draw = ImageDraw.Draw(image3)
font = ImageFont.truetype("LiberationMono-Regular.ttf", 30)
heading_font = ImageFont.truetype("LiberationMono-Regular.ttf", 40)
draw.fontmode = "1"


class IdentityDict(dict):
	def __missing__(self, key):
		return key


ncs = list((k, v) for k, v in religion_colors.items())
ncs.sort(key = lambda k: k[0])

if religions_loc is not None:
	ncs2 = []
	display_names = {}
	for path in glob.glob(religions_loc + "/**", recursive = True):
		if not os.path.isfile(path):
			continue
		with open(path, "r") as f:
			for line in f.readlines():
				if ":" in line and "collective" not in line and "group" not in line:
					tokens = line.split()
					identifier = tokens[0][:-2]
					if identifier in religion_families.keys():
						name = "".join(t + " " for t in tokens[1:])[1:-2]
						display_names[identifier] = name
						continue
					if not identifier.endswith("_adj"):
						continue
					real_id = identifier[:-4]
					if real_id not in religion_colors.keys():
						continue
					name = "".join(t + " " for t in tokens[1:])[1:-2]
					ncs2.append((real_id, religion_colors[real_id]))
					display_names[real_id] = name
	ncs = ncs2
else:
	display_names = IdentityDict()

ncs3 = []
seen = set()
for k, v in religion_families.items():
	#print(k, v)
	ncs3.append((k, (), True))
	for name in v:
		if name not in seen:
			ncs3.append((name, religion_colors[name], False))
			seen.add(name)

ncs = ncs3

index = 0
for code_name, color, is_heading in ncs:
	if code_name in used_religions.keys() or is_heading:
		if is_heading and not any(r in used_religions.keys() for r in religion_families[code_name]):
			continue
		name = display_names[code_name]
		height = (image.size[1] - 200) // 50
		xpos = 550 * (index // height) + 100
		ypos = 50 * (index % height) + 100
		if not is_heading:
			draw.rectangle(xy = [xpos, ypos, xpos + 40, ypos + 40], fill = color, outline = (0, 0, 0), width = 2)
		draw.text((xpos + (0 if is_heading else 55), ypos),
						name,
						fill = (0, 0, 0),
						font = heading_font if is_heading else font
		)
		index += 1

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[ReligionMapper] Took " + str(round(elapsed_time * 1000)) + " ms for image")

#image2.show()
image2.save(output, "PNG")
image3.save(names_output, "PNG")