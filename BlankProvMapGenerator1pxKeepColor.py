#!/usr/bin/env python3

import os
import sys
import timeit
from PIL import Image

BORDER_COLOR = (0, 0, 0)

image_file = sys.argv[1]
output = sys.argv[2]
Image.MAX_IMAGE_PIXELS *= 2 # Unsafe

print(f"[BorderFiller] Opening image at {image_file} and outputting to {output}")
image = Image.open(image_file)
image2 = Image.new('RGB', (image.size[0], image.size[1]), color = 'red')

start_time = timeit.default_timer()

pixels = image.load()
pixels2 = image2.load()
for i in range(image.size[0]):
	for j in range(image.size[1]):
		if i > 0 and i < image.size[0]-1 and j > 0 and j < image.size[1]-1:
			if (((pixels[i,j] != pixels[i+1,j]) and (pixels2[i+1,j] != BORDER_COLOR)) or ((pixels[i,j] != pixels[i,j+1]) and (pixels2[i,j+1] != BORDER_COLOR))):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == 0 and j == 0:
			if (pixels[i,j] != pixels[i+1,j] and pixels2[i+1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j+1] and pixels2[i,j+1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == image.size[0]-1 and j == image.size[1]-1:
			if (pixels[i,j] != pixels[i-1,j] and pixels2[i-1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j-1] and pixels2[i,j-1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == 0 and j == image.size[1]-1:
			if (pixels[i,j] != pixels[i+1,j] and pixels2[i+1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j-1] and pixels2[i,j-1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == image.size[0]-1 and j == 0:
			if (pixels[i,j] != pixels[i-1,j] and pixels2[i-1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j+1] and pixels2[i,j+1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == 0:
			if (pixels[i,j] != pixels[i+1,j] and pixels2[i+1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j+1] and pixels2[i,j+1] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j-1] and pixels2[i,j-1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif i == image.size[0]-1:
			if (pixels[i,j] != pixels[i-1,j] and pixels2[i-1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j+1] and pixels2[i,j+1] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j-1] and pixels2[i,j-1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif j == 0:
			if (pixels[i,j] != pixels[i-1,j] and pixels2[i-1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i+1,j] and pixels2[i+1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j+1] and pixels2[i,j+1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
		elif j == image.size[1]-1:
			if (pixels[i,j] != pixels[i-1,j] and pixels2[i-1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i+1,j] and pixels2[i+1,j] != BORDER_COLOR) or (pixels[i,j] != pixels[i,j-1] and pixels2[i,j-1] != BORDER_COLOR):
				pixels2[i,j] = BORDER_COLOR
			else:
				pixels2[i,j] = pixels[i,j]
#

end_time = timeit.default_timer()
elapsed_time = end_time - start_time
print("[BorderFiller] Took " + str(round(elapsed_time * 1000)) + " ms")

#image2.show()
image2.save(output, "PNG")





















