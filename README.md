Programs to generate blank province maps from Paradox-style province maps.

To run, add the filenames for your input and output to the program you want to use and run them using Python.

Requires PIL.

# AtE Map Script

Generates a series of maps for AtE and presumably other CK3 mods as well (haven't tested).

Invoke as `./gen-all-ate.sh /path/to/ate /path/to/output`, wrapping the paths in quotes if they have spaces, not using `~`, and avoiding a trailing slash.

The output folder should contain a `realms.png` for the Realms map; it will error otherwise.

Tested on Linux with ImageMagick 6.9.11-60 Q16, Python 3.10.6; probably doesn't work on Windows and maybe not on Mac either but I can't test on those.