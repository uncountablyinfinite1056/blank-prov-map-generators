#!/bin/bash

echo "Using $1 as mod directory"
echo "Using $2 as output directory"

# Start with ProvCleaner to create overlay map
echo "Creating overlay"
./ProvCleanerCK3.py "$1" "$2/overlay.png" --only-special

# Run county collator for counties-bad
echo "Creating county collation"
./CountyCollator.py "$1" "$2/counties-bad.png"

# Overlay onto counties-bad for C-1
# -define used to ensure we have the right TrueColor, rather than indexed, since the latter breaks the bordermaker
# https://legacy.imagemagick.org/discourse-server/viewtopic.php?t=15445
echo "Creating county borderless"
convert "$2/counties-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/counties-borderless.png"

# Overlay onto provinces.~~bmp~~png for B-1
echo "Creating provinces borderless"
convert "$1/map_data/provinces.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/provinces-borderless.png"

# Create B-2 using bordermaker
echo "Creating provinces bordered"
./BlankProvMapGenerator1pxKeepColor.py "$2/provinces-borderless.png" "$2/provinces-bordered.png"

# Extract borders from it: https://stackoverflow.com/questions/66891869/show-only-all-non-black-non-white-pixels-with-imagemagick
echo "Creating province border separate image"
convert "$2/provinces-bordered.png" -colorspace rgb +transparent black "$2/province-borders.png"

# Create C-2 using bordermaker
echo "Creating counties bordered"
./BlankProvMapGenerator1pxKeepColor.py "$2/counties-borderless.png" "$2/counties-bordered.png"

# Extract borders from it: https://stackoverflow.com/questions/66891869/show-only-all-non-black-non-white-pixels-with-imagemagick
echo "Creating county border separate image"
convert "$2/counties-bordered.png" -colorspace rgb +transparent black "$2/counties-borders.png"

# Probably won't do B/W because I don't care enough to figure out the magick

# Create duchies-borderless
echo "Creating duchies"
./DuchyCollator.py "$1" "$2/duchies-bad.png"
convert "$2/duchies-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/duchies-borderless.png"
# ...and duchies-bordered
./BlankProvMapGenerator1pxKeepColor.py "$2/duchies-borderless.png" "$2/duchies-bordered.png"


# Create kingdoms-borderless
echo "Creating kingdoms"
./KingdomCollator.py "$1" "$2/kingdoms-bad.png"
convert "$2/kingdoms-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/kingdoms-borderless.png"
# ...and kingdoms-bordered
./BlankProvMapGenerator1pxKeepColor.py "$2/kingdoms-borderless.png" "$2/kingdoms-bordered.png"


# Create empires-borderless
echo "Creating empires"
./EmpireCollator.py "$1" "$2/empires-bad.png"
convert "$2/empires-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/empires-borderless.png"
# ...and empires-bordered
./BlankProvMapGenerator1pxKeepColor.py "$2/empires-borderless.png" "$2/empires-bordered.png"


# Create cultures-borderless
echo "Creating cultures"
./CultureMapper.py "$1" "$2/cultures-bad.png" "$2/cultures-names.png" "$1/localization/english/replace/culture"
convert "$2/cultures-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/cultures-borderless.png"
# Borderize
./BlankProvMapGenerator1pxKeepColor.py "$2/cultures-borderless.png" "$2/cultures-bordered.png"
# Add names
convert "$2/cultures-borderless.png" "$2/cultures-names.png" -define png:color-type=2 +append "$2/cultures-borderless.png"
convert "$2/cultures-bordered.png" "$2/cultures-names.png" -define png:color-type=2 +append "$2/cultures-bordered.png"
convert "$2/cultures-borderless.png" "$2/province-borders.png" -define png:color-type=2 -gravity West -composite "$2/cultures-provborder.png"


# Create religions-borderless
echo "Creating religions"
./ReligionMapper.py "$1" "$2/religions-bad.png" "$2/religions-names.png" "$1/localization/english/replace/religion"
convert "$2/religions-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/religions-borderless.png"
./BlankProvMapGenerator1pxKeepColor.py "$2/religions-borderless.png" "$2/religions-bordered.png"
# Add names
convert "$2/religions-borderless.png" "$2/religions-names.png" -define png:color-type=2 +append "$2/religions-borderless.png"
convert "$2/religions-bordered.png" "$2/religions-names.png" -define png:color-type=2 +append "$2/religions-bordered.png"
convert "$2/religions-borderless.png" "$2/province-borders.png" -define png:color-type=2 -gravity West -composite "$2/religions-provborder.png"

echo "Creating holdings"
./HoldingMapper.py "$1" "$2/holdings-bad.png"
convert "$2/holdings-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/holdings-borderless.png"
./BlankProvMapGenerator1pxKeepColor.py "$2/holdings-borderless.png" "$2/holdings-bordered.png"
convert "$2/holdings-borderless.png" "$2/province-borders.png" -define png:color-type=2 -gravity Center -composite "$2/holdings-provborder.png"
convert "$2/holdings-borderless.png" "$2/counties-borders.png" -define png:color-type=2 -gravity Center -composite "$2/holdings-countyborder.png"

echo "Creating special buildings"
./SpecialBuildingMapper.py "$1" "$2/sbuildings-bad.png"
convert "$2/sbuildings-bad.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/sbuildings-borderless.png"
convert "$2/sbuildings-borderless.png" "$2/province-borders.png" -define png:color-type=2 -gravity Center -composite "$2/sbuildings-map.png"

echo "Creating realms"
convert "$2/realms.png" "$2/overlay.png" -define png:color-type=2 -gravity Center -composite "$2/realms-borderless.png"
./BlankProvMapGenerator1pxKeepColor.py "$2/realms-borderless.png" "$2/realms-bordered.png"
convert "$2/realms-borderless.png" "$2/province-borders.png" -define png:color-type=2 -gravity Center -composite "$2/realms-provborder.png"
convert "$2/realms-borderless.png" "$2/counties-borders.png" -define png:color-type=2 -gravity Center -composite "$2/realms-countyborder.png"

rm $2/*-bad.png
rm $2/*-borders.png
rm $2/*-names.png
rm "$2/sbuildings-borderless.png"
rm "$2/overlay.png"